//
//  TableViewModel.swift
//  Schultopf
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class EmployeeService {
    func getEmployees() -> Observable<[String]> {
        return Observable.just(["Stefan", "Malte", "Paul", "Julian", "Sebastian", "Quirin", "Joan"])
            .delay(3.0, scheduler: MainScheduler.instance)
    }
}

protocol EmployeesViewModelInput {
    var filterText: BehaviorSubject<String> { get }
}

protocol EmployeesViewModelOutput {
    var employees: Observable<[String]> { get }
    var isLoading: Observable<Bool> { get }
}

protocol EmployeesViewModel {
    var input: EmployeesViewModelInput { get }
    var output: EmployeesViewModelOutput { get }

    func detailViewModel(for employee: String) -> EmployeeDetailViewModel
}

class EmployeesViewModelImpl: EmployeesViewModel, EmployeesViewModelInput, EmployeesViewModelOutput {

    var input: EmployeesViewModelInput { return self }
    var output: EmployeesViewModelOutput { return self }

    // Private
    private let employeeService: EmployeeService

    init(employeeService: EmployeeService = EmployeeService()) {
        self.employeeService = employeeService
    }

    // Inputs
    let filterText = BehaviorSubject(value: "")

    // Outputs
    lazy var employees = Observable.combineLatest(filterText, employeeService.getEmployees())
        .map { filterText, allEmployees -> [String] in
            guard !filterText.isEmpty else { return allEmployees }

            return allEmployees.filter { $0.contains(filterText) }
        }

    lazy var isLoading = employees
        .map { _ in false }
        .startWith(true)

    // Child view models
    func detailViewModel(for employee: String) -> EmployeeDetailViewModel {
        return EmployeeDetailViewModelImpl(employee: employee)
    }
}
