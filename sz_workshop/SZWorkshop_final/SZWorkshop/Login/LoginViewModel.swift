//
//  LoginViewModel.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 08.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginViewModel {
    var inputEmailText: BehaviorSubject<String> { get }
    var inputPasswordText: BehaviorSubject<String> { get }
    var inputLoginButtonTrigger: PublishSubject<Bool> { get }

    var outputIsEmailValid: Observable<Bool> { get }
    var outputIsPasswordValid: Observable<Bool> { get }
    var outputIsLoginButtonEnabled: Observable<Bool> { get }
}
