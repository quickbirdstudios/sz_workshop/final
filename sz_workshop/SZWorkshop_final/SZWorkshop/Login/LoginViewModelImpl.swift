//
//  LoginViewModelImpl.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 09.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift

protocol UserService {}
class UserServiceImpl: UserService {}

class ValidationUtils {
    static func isEmailValid(_ string: String) -> Bool { return !string.isEmpty }
    static func isPasswordValid(_ string: String) -> Bool { return !string.isEmpty }
}

class LoginViewModelImpl: LoginViewModel {

    private let userService: UserService

    init(userService: UserServiceImpl) {
        self.userService = userService
    }

    // MARK: - Inputs

    let inputEmailText: BehaviorSubject<String> = BehaviorSubject(value: "")
    let inputPasswordText: BehaviorSubject<String> = BehaviorSubject(value: "")
    let inputLoginButtonTrigger: PublishSubject<Bool> = PublishSubject()

    // MARK: - Outputs

    lazy var outputIsLoginButtonEnabled: Observable<Bool> =
        Observable.combineLatest(outputIsEmailValid, outputIsPasswordValid)
            .map { isEmailValid, isPasswordField in isEmailValid && isPasswordField }

    lazy var outputIsPasswordValid: Observable<Bool> =
        inputPasswordText
            .map(ValidationUtils.isPasswordValid)

    lazy var outputIsEmailValid: Observable<Bool> =
        inputEmailText
            .map(ValidationUtils.isEmailValid)

}
