//
//  AppDelegate.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        let vc = EmployeesViewController()
        vc.viewModel = EmployeesViewModelImpl()

        window?.rootViewController = UINavigationController(rootViewController: vc)

        return true
    }


}

extension Observable {

    static func swapBetween(_ state1: Element, _ state2: Element, interval: Double = 0.5) -> Observable<Element> {
        return Observable<Int>
            .interval(interval * 2, scheduler: MainScheduler.instance)
            .flatMap { _ -> Observable<Element> in
                return Observable.just(state1)
                    .concat(Observable.just(state2).delay(interval, scheduler: MainScheduler.instance))
            }
    }

}

