//
//  UIButton+Color.swift
//  Fahrerclub
//
//  Created by Stefan Kofler on 07.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import UIKit

@IBDesignable extension UIButton {

    @IBInspectable var color: UIColor? {
        get {
            return backgroundColor
        }
        set {
            guard let color = newValue else { return }
            backgroundColor = UIColor.clear

            let bgImage = UIImage(color: color)
            setBackgroundImage(bgImage, for: .normal)

            let bgImageDisabled = UIImage(color: color.withAlphaComponent(0.5))
            setBackgroundImage(bgImageDisabled, for: .disabled)
        }
    }

}
