//
//  UIView+Extentions.swift
//  Fahrerclub
//
//  Created by Joan Disho on 06.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    func round(withCornerRadius cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }

    func applyShadow(radius: CGFloat = 4.0, opacity: CGFloat = 0.5, offset: CGSize = CGSize.zero, color: UIColor = UIColor.black) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = Float(opacity)
        self.layer.masksToBounds = false
    }

}
