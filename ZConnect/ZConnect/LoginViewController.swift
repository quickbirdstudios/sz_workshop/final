//
//  LoginViewController.swift
//  ZConnect
//
//  Created by Stefan Kofler on 12.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet private var usernameField: UITextField!
    @IBOutlet private var passwordField: UITextField!
    @IBOutlet private var loginButton: FCButton!

    // MARK: - Init

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }

    private func configureUI() {
        loginButton.round(withCornerRadius: 5)

        usernameField.addTarget(self, action: #selector(focusPasswordField), for: .editingDidEndOnExit)
        passwordField.addTarget(self, action: #selector(loginButtonPressed), for: .editingDidEndOnExit)
    }

    // MARK: Actions

    @IBAction func loginButtonPressed() {
        view.endEditing(true)

        let username = usernameField.text ?? ""
        let password = passwordField.text ?? ""
        PasswordService.storeCredentials(username: username, password: password)

        let credential = URLCredential(user: username, password: password, persistence: .forSession)
        let protectionSpace = URLProtectionSpace(host: "zconnect.zeppelin.com", port: 443, protocol: "https", realm: "Restricted", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
        URLCredentialStorage.shared.setDefaultCredential(credential, for: protectionSpace)

        let urlSession = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)

        let address = "https://zconnect.zeppelin.com"
        let url = URL(string: address)!

        loginButton.isLoading = true

        urlSession.dataTask(with: url) { [weak self] (data, response, error) in
            DispatchQueue.main.async { [weak self] in
                self?.loginButton.isLoading = false
                let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
                if 200..<300 ~= statusCode {
                    self?.showBrowser()
                } else {
                    PasswordService.deleteCredentials()
                    self?.showErrorAlert()
                }
            }
        }.resume()
    }

    // MARK: Helpers

    private func showErrorAlert() {
        let alertController = UIAlertController(title: "Fehler bei der Anmeldung", message: "Bitte überprüfen Sie Ihre Eingaben und versuchen es später erneut.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    private func showBrowser() {
        let browserViewController = self.storyboard?.instantiateViewController(withIdentifier: "BrowserNavigationController")
        let keyWindow = UIApplication.shared.keyWindow
        keyWindow?.rootViewController = browserViewController
    }

    @objc private func focusPasswordField() {
        passwordField.becomeFirstResponder()
    }

}

// MARK: - URLSessionDelegate

extension LoginViewController: URLSessionDelegate {

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard challenge.previousFailureCount <= 1 else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }

        let (username, password) = PasswordService.getCredentials()
        let credentials = URLCredential(user: username, password: password, persistence: .forSession)
        completionHandler(.useCredential, credentials)
    }

}
