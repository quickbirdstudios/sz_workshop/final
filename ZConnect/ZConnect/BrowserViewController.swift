//
//  BrowserViewController.swift
//  ZConnect
//
//  Created by Stefan Kofler on 12.02.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import UIKit
import WebKit

class BrowserViewController: UIViewController {

    @IBOutlet private var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Z CONNECT"
        webView.navigationDelegate = self

        let url = getURL()
        webView.load(URLRequest(url: url))
    }

    // MARK: Actions

    @IBAction func logoutButtonPressed() {
        let alertController = UIAlertController(title: "Wollen Sie sich wirklich abmelden?", message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Abmelden", style: .destructive, handler: { [weak self] _ in
            self?.logout()
        }))
        alertController.addAction(UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: Helpers

    private func getURL() -> URL {
        let url = URL(string: "https://zconnect.zeppelin.com")
        return url!
    }

    private func logout() {
        PasswordService.deleteCredentials()

        let loginViewController = self.storyboard?.instantiateInitialViewController()
        let keyWindow = UIApplication.shared.keyWindow
        keyWindow?.rootViewController = loginViewController
    }

}

extension BrowserViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        guard let statusCode = (navigationResponse.response as? HTTPURLResponse)?.statusCode else {
            decisionHandler(.allow)
            return
        }

        if statusCode == 401 {
            logout()
        }

        decisionHandler(.allow)
    }

    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard challenge.previousFailureCount <= 1 else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            logout()
            return
        }

        let (username, password) = PasswordService.getCredentials()
        let credentials = URLCredential(user: username, password: password, persistence: .forSession)
        completionHandler(.useCredential, credentials)
    }

}

